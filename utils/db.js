const mongoose = require("mongoose");

const user = 'dev3pgonzalez';
const password = '12345_';

const DB_URL = `mongodb+srv://${user}:${password}@cluster0.navam8i.mongodb.net/?retryWrites=true&w=majority`;

mongoose.connect(DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})


/* const connect = async () => {
  try {
    const db = await mongoose.connect(mongoDb, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    const { name, host } = db.connection;
    console.log(`Conectado a la DB : ${name} en el host: ${host}`);
  } catch (error) {
    console.error(`No se ha podido conectar a la DB `, error);
  }
};
 */
module.exports = { connect };