
const books = {
    '1': { 'title': 'El Quijote' },
    '2': { 'title': 'La Iliada' },
    '3': { 'title': 'La Odisea' },
    '4': { 'title': 'La Divina Comedia' }
}

const people = {
    'Antonio': ['videojuegos', 'pescar', 'motos'],
    'Paco': ['senderismo', 'viajar', 'terrazas'],
    'Luis': ['motos', 'conducir', 'aviones'],
    'Maria': ['motos', 'viajar', 'pescar'],
    'Laura': ['coches', 'pilotar', 'trenes']
}

const getBooks = (req, res) => {
    res.status(200);
}

const getBookById = (req, res) => {
    return books[req.params.id];
}

const getHobbieByIdAndName = (req, res) => {
    const hobbies = [];
    for (let name in people) {
        hobbies.push(people[name][req.params.id])
    }
    res.send(JSON.stringify(hobbies)) ;
}

module.exports = {
    getBooks,
    getBookById,
    getHobbieByIdAndName
};