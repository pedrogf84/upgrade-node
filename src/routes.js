const router = require("express").Router();

const {
    getBooks,
    getBookById,
    getHobbieByIdAndName
} = require("./controller");

router.get("/books", getBooks);
router.get('/book/:id', getBookById);
router.get('/hobbie/:id', getHobbieByIdAndName)

module.exports = router;